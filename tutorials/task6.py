import asyncio


async def factorial(name, number):
    f = 1
    for i in range(2, number + 1):
        print(f"Task {name}: Compute factorial({i})...")
        await asyncio.sleep(1)
        f *= i
    print(f"Task {name}: factorial({number}) = {f}")


async def main():
    args = [('A', 2), ('B', 3), ('C', 4)]
    tasks = []
    for arg in args:
        # создаем задачи
        task = factorial(*arg)
        # складываем задачи в список
        tasks.append(task)

    # планируем одновременные вызовы
    await asyncio.gather(*tasks)


if __name__ == '__main__':
    # Запускаем цикл событий
    results = asyncio.run(main())
