from abc import abstractmethod
from pathlib import Path
import pandas as pd
from jinja2 import Environment, FileSystemLoader


class DataToHtmlConvertor:
    def __init__(self, file_path, town, html_path=Path(__file__).parent, create_file=True, need_open=True, **kwargs):
        self.file_path = file_path
        self.town = town.strip()
        self.dataframe = None

        self.create_file = create_file
        if self.create_file:
            self.html_path = html_path / f'{self.town}.html'

        self.need_open = need_open

        self.chunk_size = kwargs.get('chunk_size')
        self.new_line = kwargs.get('new_line')

    def processing(self):
        """
        Обработка файла и превращение данных из него в html.
        :return: html данные.
        """
        try:
            if self.need_open:
                with open(self.file_path, newline=self.new_line, encoding='utf-8') as file:
                    self.get_sorted_data(file)
            else:
                self.get_sorted_data(self.file_path)
        except Exception as exp:
            print(str(exp) + '\n')

        try:
            data = self.create_html()
            if self.create_file:
                with open(self.html_path, 'w', encoding='utf-8') as file:
                    template_env = Environment(loader=FileSystemLoader(str(Path(__file__).parent / 'templates')))
                    template = template_env.get_template('weather.html')
                    file.write(template.render(data))
                html_data = 'File created'
            else:
                template_env = Environment(loader=FileSystemLoader(str(Path(__file__).parent / 'templates')))
                template = template_env.get_template('weather.html')
                html_data = template.render(data)
        except Exception as exp:
            print(str(exp) + '\n')
            html_data = None

        return html_data

    @abstractmethod
    def get_sorted_data(self, file):
        """
        Реализуется в дочерних классах обязательно.
        Читает и сортирует данные из файла.
        :param self:
        :param file:
        :return:
        """
        pass

    def create_html(self):
        """
        Создает данные html используя данные из dataframe.
        :return: Словарь data для шаблона из jinja.
        """
        if self.dataframe is not None:
            self.dataframe.reset_index(drop=True, inplace=True)
            headers = list(self.dataframe.columns.values)
            rows = []
            for i in range(len(self.dataframe)):
                rows.append(self.dataframe.loc[i])
            data = {'rows': rows, 'headers': headers, 'town': self.town}
        else:
            raise Exception('self.dataframe is None')

        return data


class CsvToHtml(DataToHtmlConvertor):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if kwargs.get('new_line'):
            self.new_line = kwargs['new_line']
        else:
            self.new_line = ''

    def get_sorted_data(self, file):
        rows_list = []
        for chunk in pd.read_csv(file, chunksize=self.chunk_size):
            new_chunk = chunk[chunk.town == self.town]
            new_chunk.reset_index(drop=True, inplace=True)
            for i in range(len(new_chunk)):
                dict_row = new_chunk.loc[i].to_dict()
                rows_list.append(dict_row)
        df = pd.DataFrame(rows_list)
        sorted_df = df.sort_values(by=['day'])
        self.dataframe = sorted_df


class JsonToHtml(DataToHtmlConvertor):
    def get_sorted_data(self, file):
        df = pd.read_json(file)
        new_df = df[df.town == self.town]
        sorted_df = new_df.sort_values(by=['day'])
        self.dataframe = sorted_df


class XlsxToHtml(DataToHtmlConvertor):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.need_open = False

    def get_sorted_data(self, file):
        df = pd.read_excel(file, engine='openpyxl')
        new_df = df[df.town == self.town]
        sorted_df = new_df.sort_values(by=['day'])
        self.dataframe = sorted_df
