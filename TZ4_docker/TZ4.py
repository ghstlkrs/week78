import asyncio
import asyncpg
import os
import pandas as pd

import data_to_html_convertor

from pathlib import Path
from dotenv import load_dotenv
from io import StringIO
from datetime import datetime

config_path = Path(__file__).parent / '.env'
load_dotenv(config_path)


def process_json(log_dict, file_path):
    """
    Читает json и создает словарь вида 'id файла: {города, по которым необходим поиск в этом файле}'.
    :param log_dict:
    :param file_path:
    :return:
    """

    source_id_to_town = {}
    try:
        with open(file_path, encoding='utf-8', mode='r') as file:
            dictlist = pd.read_json(file).to_dict('records')
            for dictionary in dictlist:
                log_dict['records_read'] += 1
                if dictionary['source_id'] in source_id_to_town:
                    source_id_to_town[(dictionary['source_id'])].append(dictionary['town'])
                else:
                    source_id_to_town[(dictionary['source_id'])] = [(dictionary['town'])]
            for key in source_id_to_town.keys():
                source_id_to_town[key] = set(source_id_to_town[key])

    except Exception as exp:
        print(str(exp) + ' IN PROCESS_JSON')
        log_dict['number_of_exceptions'] += 1

    return source_id_to_town


async def select_query(log_dict, source_id):
    """
    Выполняет select запрос по source_id к БД.
    :param log_dict:
    :param source_id:
    :return:
    """
    select_query = '''select source_filename, source_filedata from source_files where source_id = $1'''
    try:
        conn = await asyncpg.connect(os.getenv('DATABASE_URL'))
        source_file_record = await conn.fetchrow(select_query, source_id)
        await conn.close()
        file_name = source_file_record[0]
        file_data = source_file_record[1].decode('utf-8')

    except Exception as exp:
        print(str(exp) + ' IN SELECT_QUERY')
        log_dict['number_of_exceptions'] += 1
        file_name, file_data = None, None

    return file_name, file_data


async def convert_to_html(log_dict, source_id_to_town):
    """
    Конвертирует полученные данные в html и вставляет полученный результат в БД.
    :param log_dict:
    :param source_id_to_town:
    :return:
    """
    insert_tasks = []
    for source_id, towns in source_id_to_town.items():
        file_name, file_data = await select_query(log_dict, source_id)
        try:
            if file_name.endswith('.csv'):
                for town in towns:
                    data = data_to_html_convertor.CsvToHtml(
                        file_path=StringIO(file_data),
                        town=town,
                        chunk_size=10,
                        need_open=False,
                        create_file=False
                    ).processing()

                    result_filename = f'{town}.html'
                    b_data = bytes(data, 'utf-8')
                    insert_tasks.append(insert_result_files(log_dict, source_id, result_filename, b_data))
                    log_dict['records_to_insert'] += 1

            elif file_name.endswith('.json'):
                for town in towns:
                    data = data_to_html_convertor.JsonToHtml(
                        file_path=file_data,
                        town=town,
                        chunk_size=10,
                        need_open=False,
                        create_file=False
                    ).processing()

                    result_filename = f'{town}.html'
                    b_data = bytes(data, 'utf-8')
                    insert_tasks.append(insert_result_files(log_dict, source_id, result_filename, b_data))
                    log_dict['records_to_insert'] += 1

            elif file_name.endswith('.xlsx'):
                for town in towns:
                    data = data_to_html_convertor.XlsxToHtml(
                        file_path=file_data,
                        town=town,
                        chunk_size=10,
                        need_open=False,
                        create_file=False
                    ).processing()

                    result_filename = f'{town}.html'
                    b_data = bytes(data, 'utf-8')
                    insert_tasks.append(insert_result_files(log_dict, source_id, result_filename, b_data))
                    log_dict['records_to_insert'] += 1

        except Exception as exp:
            print(str(exp) + ' IN CONVERT_TO_HTML')
            log_dict['number_of_exceptions'] += 1

    await asyncio.gather(*insert_tasks)


async def insert_result_files(log_dict, source_id, result_filename, result_filedata):
    """
    Выполняет insert запрос к БД.
    :param log_dict:
    :param source_id:
    :param result_filename:
    :param result_filedata:
    :return:
    """
    conn = await asyncpg.connect(os.getenv('DATABASE_URL'))
    insert_query = '''
        insert into result_files(source_id, result_filename, result_filedata) values ($1, $2, $3)'''
    # await conn.execute(insert_query, source_id, result_filename, result_filedata)
    await conn.close()

    log_dict['records_inserted'] += 1


async def create_report(log_dict, json_name):
    file_path = Path(__file__).parent / 'settings' / 'data_out' / \
                f'{json_name[:json_name.find(".json")]}_report.txt'
    try:
        with open(file_path, encoding='utf-8', mode='w') as file:
            file.write(f'{json_name}\n{datetime.now().strftime("%Y-%m-%d %H:%M:%S")}\n')
            table = str(pd.DataFrame.from_dict(log_dict, orient='index'))
            file.write(table)

    except Exception as exp:
        print(str(exp) + ' IN CREATE_REPORT')


async def main(json_name):
    file_path = Path(__file__).parent / 'settings' / 'data_in' / json_name
    log_dict = {'records_read': 0,
                'records_to_insert': 0,
                'records_inserted': 0,
                'number_of_exceptions': 0}
    process_dict = process_json(log_dict, file_path)
    await convert_to_html(log_dict, process_dict)
    print(log_dict)
    await create_report(log_dict, json_name)
    log_dict.clear()
    os.replace(file_path, Path(__file__).parent / 'settings' / 'done' / json_name)


if __name__ == '__main__':
    asyncio.run(main('main.json'))
